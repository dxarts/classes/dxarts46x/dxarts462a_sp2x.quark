/*
Course: DXARTS 462a Spring 2023 (https://canvas.uw.edu/courses/1633437)
Authors: Joseph Anderson, Dan Peterson, James Wenlock, et al.
Affiliation: DXARTS, University of Washington (https://dxarts.washington.edu/)
License: GPLv3

Monophonic PlayBuf with Convolution2
*/

({ |main|
	{ |dur, gain = 0.0, ris = 0.01, dec = 0.01, rate = 1, loop = 0, panAngle = 0.0, soundBuf = 0, kernelBuf = 1, framesize = 2048|

		// variables
		var bus;          // var to specify output bus
		var numChannels = 1; // <-- mono buffer
		var playBuf, filtered, out;     // vars assigned to audio signals
		var amp;  // a few vars for synthesis
		var preAmpEnv, postAmpEnv;       // vars for envelope signal


		// assign values
		bus = 0;          // first output

		// calcs
		amp = gain.dbamp; // convert from gain in dB to linear amplitude scale

		// the amplitude envelopes
		/*
		NOTE:

		Envelope before and after to account for delay caused by Convolution2
		*/
		preAmpEnv = EnvGen.kr(
			Env.new(
				levels: [0, 1, 1],
				times: [ris, 1 - ris]
			),
			timeScale: dur
		);
		postAmpEnv = EnvGen.kr(
			Env.new(
				levels: [1, 1, 0],
				times: [1 - dec, dec]
			),
			timeScale: dur
		);

		// sample playback
		playBuf = PlayBuf.ar(numChannels, soundBuf,  BufRateScale.kr(soundBuf) * rate, loop: loop);

		// pre envelope
		playBuf = preAmpEnv * playBuf;

		// filter
		filtered = Convolution2.ar(playBuf, kernelBuf, framesize: framesize); // <-- Subtractive Synthesis

		// post envelope & rescale by amp
		filtered = amp * postAmpEnv * filtered;

		// expand to two channels - panning
		out = main.functions[\sinCosPanLaw].value(panAngle) * filtered;  // <-- Panning happens here!

		// out!!
		Out.ar(bus, out)
	}
})
