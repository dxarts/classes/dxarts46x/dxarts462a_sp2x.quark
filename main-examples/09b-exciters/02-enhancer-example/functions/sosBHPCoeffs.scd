/*
Course: DXARTS 462a Spring 2023 (https://canvas.uw.edu/courses/1633437)
Authors: Joseph Anderson, Dan Peterson, James Wenlock, et al.
Affiliation: DXARTS, University of Washington (https://dxarts.washington.edu/)
License: GPLv3

High-pass filter coefficients - SOS

args: freq, damping, sampleRate
*/
({ |freq, d, sampleRate|
    var c;
    var a0, a1, a2, b1, b2;

    c = tan(pi * freq / sampleRate);

	a0 = 1/(1 + (2 * d * c) + (c.squared));
	a1 = -2 * a0;
	a2 = a0;
	b1 = 2 * a0 * (1 - (c.squared));
	b2 = -1 * a0 * (1 - (2 * d * c) + (c.squared));


    Array.with(a0, a1, a2, b1, b2);
})
