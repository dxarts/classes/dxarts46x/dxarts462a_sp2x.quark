/*
Course: DXARTS 462a Spring 2023 (https://canvas.uw.edu/courses/1633437)
Authors: Joseph Anderson, Dan Peterson, James Wenlock, et al.
Affiliation: DXARTS, University of Washington (https://dxarts.washington.edu/)
License: GPLv3

Monophonic PlayBuf - with Weaver SSB, LP order = 32
*/

({ |main|
	{| dur, gain = 0.0, ris = 0.01, dec = 0.01, rate = 1, panAngle = 0.0, ssbFreq = 1.0, bw = 0.95, loop = 0, buffer = 0|

		// variables
		var bus;          // var to specify output bus
		var numChannels = 1; // <-- mono buffer
		var playBuf, ssb, out;     // vars assigned to audio signals
		var quadOsc0, quadOsc1;
		var amp;  // a few vars for synthesis
		var ampEnv;       // vars for envelope signal
		var sampleRateDiv4, cascadeCoeffs;
		var n = 32; // filter order --> roll off = 32 * -6dB / octave = -192 dB / octave


		// assign values
		bus = 0;          // first output

		// calcs
		amp = gain.dbamp; // convert from gain in dB to linear amplitude scale

		sampleRateDiv4 = SampleRate.ir/4; // SSB calcs
		cascadeCoeffs = main.functions[\sosBLPCascadeCoeffs].value(
			main: main,
			freq: bw * sampleRateDiv4,
			n: n,
			sampleRate: SampleRate.ir
		); // low pass filter calcs


		// amplitude envelope: Env nested in EnvGen
		ampEnv = EnvGen.kr(
			Env.linen(ris, 1.0 - (ris + dec), dec),  // <-- envelope
			timeScale: dur
		);

		// sample playback
		playBuf = PlayBuf.ar(numChannels, buffer,  BufRateScale.kr(buffer) * rate, loop: loop);


		// Weaver SSB - quadrature oscillators
		quadOsc0 = SinOsc.ar(sampleRateDiv4, [pi/2, 0]);
		quadOsc1 = SinOsc.ar(sampleRateDiv4 + ssbFreq, [pi/2, 0]);

		// Weaver SSB - quadrature RM (0), lowpass filter, quadrature RM (1)
		ssb = playBuf * quadOsc0;  // <-- quadrature RM (0)
		cascadeCoeffs.do({ |coeffs|  /// <-- cascade SOS (lowpass)
			ssb = SOS.ar(ssb, coeffs.at(0), coeffs.at(1), coeffs.at(2), coeffs.at(3), coeffs.at(4))
		});
		ssb = ssb * quadOsc1;  // <-- quadrature RM (1)
		ssb = 2 * ssb.sum;  //   // <-- sum and scale (restore gain)

		// envelope
		ssb = ampEnv * amp * ssb;


		// spatial filter - pan
		out = main.functions[\spatMatFilt].value(
			ssb,
			main.functions[\sinCosPanLawMatrix].value(panAngle)
		);


		// out!!
		Out.ar(bus, out)
	}
})
