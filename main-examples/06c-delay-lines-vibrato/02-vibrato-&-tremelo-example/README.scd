/*
Title: 02-vibrato-&-tremelo-example

Course: DXARTS 462a Spring 2023 (https://canvas.uw.edu/courses/1633437)
Authors: Joseph Anderson, Dan Peterson, James Wenlock, et al.
Affiliation: DXARTS, University of Washington (https://dxarts.washington.edu/)
License: GPLv3

*/

/*
Main Paradigm example:

06c-Delay-Lines-Vibrato
-->Vibrato & Tremelo Example

*/