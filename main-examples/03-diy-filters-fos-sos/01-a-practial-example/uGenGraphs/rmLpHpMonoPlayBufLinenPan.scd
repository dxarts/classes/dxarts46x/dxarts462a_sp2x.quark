/*
Course: DXARTS 462a Spring 2023 (https://canvas.uw.edu/courses/1633437)
Authors: Joseph Anderson, Dan Peterson, James Wenlock, et al.
Affiliation: DXARTS, University of Washington (https://dxarts.washington.edu/)
License: GPLv3

Monophonic PlayBuf with Regalia-Mitra Low-Pass (left) & High-Pass (right)
*/

({ |main|
	{| dur, gain = 0.0, ris = 0.01, dec = 0.01, freq = 440.0, rate = 1, panAngle = 0.0, loop = 0, buffer = 0|

		// variables
		var bus;          // var to specify output bus
		var numChannels = 1; // <-- mono buffer
		var playBuf, allpass, lowpass, highpass, out;     // vars assigned to audio signals
		var amp;  // a few vars for synthesis
		var ampEnv;  // var for envelope signal
		var a0, a1, b1;  // all-pass filter coefficients

		// assign values
		bus = 0;          // first output

		// calcs
		amp = gain.dbamp; // convert from gain in dB to linear amplitude scale
		#a0, a1, b1 = main.functions[\fosAPCoeffs].value(freq, SampleRate.ir);  // filter coefficients

		// amplitude envelope: Env nested in EnvGen
		ampEnv = EnvGen.kr(
			Env.linen(ris, 1.0 - (ris + dec), dec),  // <-- envelope
			timeScale: dur
		);

		// sample playback
		playBuf = PlayBuf.ar(numChannels, buffer,  BufRateScale.kr(buffer) * rate, loop: loop);

		// All-Pass filter
		allpass = FOS.ar(playBuf, a0, a1, b1);

		// Regalia-Mitra Low-Pass & High-Pass filters
		lowpass = 0.5 * (playBuf + allpass);
		highpass = 0.5 * (playBuf - allpass);

		// group and then rescale, by multiplying by ampEnv
		// Low-Pass on left & High-Pass on right
		out = ampEnv * amp * Array.with(lowpass, highpass);

		// expand to two channels - panning
		out = main.functions[\sinCosPanLaw].value(panAngle) * out;  // <-- Panning happens here!

		// out!!
		Out.ar(bus, out)
	}
})
