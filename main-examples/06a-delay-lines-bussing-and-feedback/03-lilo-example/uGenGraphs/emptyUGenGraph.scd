/*
Course: DXARTS 462a Spring 2023 (https://canvas.uw.edu/courses/1633437)
Authors: Joseph Anderson, Dan Peterson, James Wenlock, et al.
Affiliation: DXARTS, University of Washington (https://dxarts.washington.edu/)
License: GPLv3

An empty uGenGraph
*/

({ |main|
	{
		var out, bus;

		out = [0, 0];
		bus = 0;

		Out.ar(bus, out);
	}
})
