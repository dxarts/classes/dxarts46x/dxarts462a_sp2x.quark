/*
Course: DXARTS 462a Spring 2023 (https://canvas.uw.edu/courses/1633437)
Authors: Joseph Anderson, Dan Peterson, James Wenlock, et al.
Affiliation: DXARTS, University of Washington (https://dxarts.washington.edu/)
License: GPLv3

Monophonic PlayBuf with "Feedback" Echo - implemented via LocalIn/LocalOut
Modified to include a mono cascade of SAP

NOTE: IIR Comb, specify delays and gains directly

NOTE: includes correction for LocalIn/LocalOut blockSize feedback delay
*/

({ |main|
	{| dur, gain = 0.0, ris = 0.01, dec = 0.01, directGain = 0.0, directPanAngle = 0.0, delayGain = 0.0, delayPanAngle = 0.0, delayTime = 0.2, maxDelayTime = 0.2, decayTime = 0.5, baseDelayTime = 0.030, foo = 1.07116253, sapDecayTime = 1.0, modDepthTime = 0.015, modFreq = 0.1, lpFreq = 3000.0, rate = 1, loop = 0, buffer = 0|

		// variables
		var bus;          // var to specify output bus
		var numChannels = 1; // <-- mono buffer
		var playBuf, delayIn, delay, feedback, out;     // vars assigned to audio signals
		var gFac;
		var blockDelay;
		var amp;  // a few vars for synthesis
		var ampEnv;  // var for envelope signal
		var directAmp, delayAmp;  // comb coefficients

		// reverb stuff!
		var numSAP;

		// assign values
		bus = 0;          // first output

		// reverb (SAP) setup... must be set here (NOT as ARG)
		numSAP = 8;
		// numSAP = 12;
		// numSAP = 32;

		// calcs
		amp = gain.dbamp; // convert from gain in dB to linear amplitude scale

		// delay (IIR Comb) filter calcs - directly scaled!
		directAmp = directGain.dbamp;  // direct scalar
		delayAmp = delayGain.dbamp;  // delay scalar

		// calculate feedback coefficient
		gFac = main.functions[\allPoleCoeff].value(delayTime, decayTime);

		// calculate blockSize delay - introduced by LocalIn/LocalOut pair
		blockDelay = ControlDur.ir;

		// amplitude envelope: Env nested in EnvGen
		ampEnv = EnvGen.kr(
			Env.linen(ris, 1.0 - (ris + dec), dec),  // <-- envelope
			timeScale: dur
		);

		// sample playback - direct
		playBuf = PlayBuf.ar(numChannels, buffer,  BufRateScale.kr(buffer) * rate, loop: loop);


		// -----------------------
		// begin feedback delay block
		/*
		equivalent to:

		delay = CombC.ar(playBuf, maxDelayTime, delayTime, decayTime);
		*/

		feedback = LocalIn.ar(numChannels);  // "receive" from local bus, blockDelay "added" internally here
		/*
		NOTE: add lowpass to feedback for Zolzer's "natural sounding comb"

		feedback = LPF.ar(feedback, ... );
		*/

		// lowpass filter
		feedback = LPF.ar(feedback, lpFreq);  // 2kHz - 20kHz

		// scale and mix
		delayIn = playBuf + (gFac * feedback);

		numSAP.do({ |i|
			delayIn = AllpassC.ar(
				delayIn,
				0.3,
				LFNoise2.kr(modFreq).range(
					baseDelayTime,
					baseDelayTime + modDepthTime),
				sapDecayTime
			);
			baseDelayTime = baseDelayTime * foo;
		});


		delay = DelayC.ar(
			delayIn,  // add scaled feedback to delay input
			maxDelayTime - blockDelay,  // shorten max delay by blockDelay
			delayTime - blockDelay  // shorten delay by blockDelay
		);
		LocalOut.ar(delay);  // "send" to local bus
		delay = DelayN.ar(delay, blockDelay, blockDelay); // blockSize make up delay

		// end feedback delay block
		// -----------------------


		// add panned direct and delay to out, & then envelope
		out = amp * ampEnv * (
			(main.functions[\sinCosPanLaw].value(directPanAngle) * directAmp * playBuf) +  // <-- direct
			(main.functions[\sinCosPanLaw].value(delayPanAngle) * delayAmp * delay)  // <-- delay
		);

		// out!!
		Out.ar(bus, out)
	}
})
