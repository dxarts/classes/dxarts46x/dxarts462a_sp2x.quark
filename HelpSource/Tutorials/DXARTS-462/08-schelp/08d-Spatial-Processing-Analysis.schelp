title:: 08d. Spatial Processing IV: Image Analysis
summary:: DXARTS 462 - Week 8
categories:: Tutorials>DXARTS>462
keyword:: DXARTS

link::Tutorials/DXARTS-462/00-DXARTS-462-TOC## 00. DXARTS 462: Digital Sound Processing::

link::Tutorials/DXARTS-462/08-schelp/08c-Spatial-Processing-Freq-Imaging## << 08c. Spatial Processing III: Frequency Dependent Imaging:: link::Tutorials/DXARTS-462/09-schelp/09a-Weaver-SSB## >> 09a. Weaver SSB::

Given stereophonic encoding via the link::Tutorials/DXARTS-461/02-schelp/02e-Visualization-Panorama-Acknowledge#Sine-cosine%20panning%20law#sine-cosine:: panorama law, it is possible to analyze a stereophonic signal to extract a number of useful features.


section::Power Analysis

For all our discussion we'll assume a signal encoded with the link::Tutorials/DXARTS-461/02-schelp/02e-Visualization-Panorama-Acknowledge#Sine-cosine%20panning%20law#sine-cosine:: law. Here's the individual coefficient expression:

image::08d_eq01.png::

Recall, the coefficients emphasis::gL:: and emphasis::gR:: are scalars applied to an input emphasis::monophonic signal:: to generate an emphasis::output stereophonic signal:::


image::08d_eq02.png::

For convience, we'll assume that the emphasis::panorama angle:: is either static or slowly varying in time. We'll also introduce a symbol to indicate time averaging: footnote::Also known as the emphasis::moving average::.::

image::08d_eq03.png::

The emphasis::time averaged power:: of the input monphonic signal is calculated:

image::08d_eq04.png::

With a signal encoded with the link::Tutorials/DXARTS-461/02-schelp/02e-Visualization-Panorama-Acknowledge#Sine-cosine%20panning%20law#sine-cosine:: law, let's review how to find the power of the encoded input signal:

image::08d_eq05.png::

What we see is that given the link::https://en.wikipedia.org/wiki/Pythagorean_trigonometric_identity##Pythagorean trigonometric identity::, the coefficients emphasis::gL:: and emphasis::gR:: drop out, and we can directly extract the emphasis::time averaged power:: of the input monphonic signal. We can re-write for clarity:

image::08d_eq06.png::

We can now write this as a function, using link::Classes/RunningSum:: to do the averaging over time:

code::
// function to calculate time-average power of a stereophonic signal
//
~tAPower = { |stereoIn, aveTime = 0.05|

	var left, right;
	var numsamp;
	var left2, right2, power;

	// convert time to numsamps
	numsamp = SampleRate.ir * aveTime;

	// extract left and right
	#left, right = stereoIn;

	// running sum of square
	left2 = RunningSum.ar(left.squared, numsamp);
	right2 = RunningSum.ar(right.squared, numsamp);

	// analyze power
	power = numsamp.reciprocal * (left2 + right2);

	// return
	power;
};
::

section::Balance Analysis

The emphasis::time averaged balance:: indicates how the power is balanced across the emphasis::left-right axis:: of the stereo field, and can be found:

image::08d_eq07.png::

And solving for the trigonometric expression:

image::08d_eq08.png::

The returned value varies between code::1:: and code::-1::and these are mapped across the image:

table::
## emphasis::position:: || strong::left:: || strong::center:: || strong::right::
## emphasis::value:: || code::1.0:: || code::0.0:: || code::-1.0::
::

As a funciton, using link::Classes/RunningSum:::

code::
~tABalance = { |stereoIn, aveTime = 0.05, regGain = -180.0|

	var left, right;
	var numsamp;
	var left2, right2, balance;
	var reg;

	// convert time to numsamps
	numsamp = SampleRate.ir * aveTime;

	// extract left and right
	#left, right = stereoIn;

	// running sum square
	left2 = RunningSum.ar(left.squared, numsamp);
	right2 = RunningSum.ar(right.squared, numsamp);

	// the regularization - avoid divide by zero!
	reg = DC.ar(regGain.dbamp);

	// analyze balance
	balance = (left2 - right2) / ((left2 + right2) + reg);

	// return
	balance;
};
::

note::We added a emphasis::regularization:: value to avoid divide by zero!::


section::Correlation Analysis

emphasis::Time averaged correlation:: indicates how the power is balanced across the emphasis::middle-side axis:: of the stereo field. We can think of this value as indicating  emphasis::how stereo:: the signal is:

image::08d_eq09.png::

And solving for the trigonometric expression:

image::08d_eq10.png::

The returned value varies between code::1:: and code::-1:: and these are mapped:

table::
## emphasis::position:: || strong::middle:: || strong::de-correlated:: || strong::side::
## emphasis::value:: || code::1.0:: || code::0.0:: || code::-1.0::
::

See Kendall's discussion as to how these values of emphasis::correlation:: audition. footnote:: Kendall, G. S. 1995. "The Decorrelation of Audio Signals and Its Impact on Spatial Imagery". COMPUTER MUSIC JOURNAL. 19 (4): 71-87.::

As a funciton:

code::
~tACorrelation = { |stereoIn, aveTime = 0.05, regGain = -180.0|

	var left, right;
	var numsamp;
	var left2, right2, multLR, correlation;
	var reg;

	// convert time to numsamps
	numsamp = SampleRate.ir * aveTime;

	// extract left and right
	#left, right = stereoIn;

	// running sum square
	left2 = RunningSum.ar(left.squared, numsamp);
	right2 = RunningSum.ar(right.squared, numsamp);

	// running sum multiply
	multLR = RunningSum.ar(left * right, numsamp);

	// the regularization - avoid divide by zero!
	reg = DC.ar(regGain.dbamp);

	// analyze balance
	correlation = 2 * multLR / ((left2 + right2) + reg);

	// return
	correlation;
};
::

section::Angle Analysis

Both link::#Balance%20Analysis#balance:: and link::#Correlation%20Analysis#correlation:: are returned to us as trigonometric expressions:

image::08d_eq11.png::

You'll notice that we can extract the emphasis::panorama angle:::

image::08d_eq12.png::

We can use link::Overviews/Operators#.atan2#atan2:::

code::
// function to calculate time-average encoding angle (in radians) of a stereophonic signal
//
~tAAngle = { |stereoIn, aveTime = 0.05, regGain = -180.0|

	var left, right;
	var numsamp;
	var left2, right2, multLR, angle;
	var reg;

	// convert time to numsamps
	numsamp = SampleRate.ir * aveTime;

	// extract left and right
	#left, right = stereoIn;

	// running sum square
	left2 = RunningSum.ar(left.squared, numsamp);
	right2 = RunningSum.ar(right.squared, numsamp);

	// running sum multiply
	multLR = RunningSum.ar(left * right, numsamp);

	// the regularization - avoid divide by zero!
	reg = DC.ar(regGain.dbamp);

	// analyze angle
	angle = 0.5 * atan2(left2 - right2, (2 * multLR) + reg);

	// return
	angle;
};
::

What use is this?

section::Synthesis & Analysis

Recall our discussion of link::Tutorials/DXARTS-461/10-schelp/10-Stereo-Synthesis#Stocastic%20signals#synthesizing broad images via stocastic sources::. SuperCollider's  link::Tutorials/DXARTS-461/09-schelp/09a-SS-Complex-Sources#Colored%20Noise#colored noise:: generators can be used to return emphasis::decorrelated:: signals. Given a emphasis::decorrelated:: emphasis::SOURCE::, we can use link::Tutorials/DXARTS-462/08-schelp/08b-Spatial-Processing-Imaging-Transforms##stereo imaging transforms:: to further shape the panorama of the generated image. In the example below we'll use link::Tutorials/DXARTS-462/08-schelp/08b-Spatial-Processing-Imaging-Transforms#Width%20Transform#width:: followed by link::Tutorials/DXARTS-462/08-schelp/08b-Spatial-Processing-Imaging-Transforms#Rotate%20Transform#rotate::.

We'll then display an analysis of link::#Balance%20Analysis#balance:: and link::#Correlation%20Analysis#correlation:: of the synthesized image in a link::Classes/Stethoscope##scope::. emphasis::Left-Right:: link::#Balance%20Analysis#balance:: is mapped to the x-axis and emphasis::Middle-Side:: link::#Correlation%20Analysis#correlation:: to the y-axis. Additionally, we'll link::Classes/UGen#-poll#poll:: the analyzed link::#Angle%20Analysis#panorama angle:: to the post window.


subsection::Realtime Example

We'll render this in realtime. Go ahead and start up the realtime server:

code::
// start server!
Server.default = s = Server.local.boot; // use this for SC-IDE
s.scope;
::

____________________

Once the realtime server is booted, evaluate the codeblock below. Notice, there are several example note calls to try.

note::Un-comment only one note example at a time!::

code::
// PinkNoise - Balance & Correlation display
(

///////////////// DEFINE VARIABLES //////////////////

var score, pinkSynthDef, analyDef;
var start, dur, gain, panAngle, widthAngle;
var dispBus, group;

// helper functions
var tABalance, tACorrelation, tAAngle, widthMatrix, rotateMatrix, spatFilt;


///////////////// DEFINE SPATIAL FUNCTIONS //////////////////

// function to calculate time-average balance of a stereophonic signal
//
tABalance = { |stereoIn, aveTime = 0.05, regGain = -180.0|

	var left, right;
	var numsamp;
	var left2, right2, balance;
	var reg;

	// convert time to numsamps
	numsamp = SampleRate.ir * aveTime;

	// extract left and right
	#left, right = stereoIn;

	// running sum square
	left2 = RunningSum.ar(left.squared, numsamp);
	right2 = RunningSum.ar(right.squared, numsamp);

	// the regularization - avoid divide by zero!
	reg = DC.ar(regGain.dbamp);

	// analyze balance
	balance = (left2 - right2) / ((left2 + right2) + reg);

	// return
	balance;
};


// function to calculate time-average correlation of a stereophonic signal
//
tACorrelation = { |stereoIn, aveTime = 0.05, regGain = -180.0|

	var left, right;
	var numsamp;
	var left2, right2, multLR, correlation;
	var reg;

	// convert time to numsamps
	numsamp = SampleRate.ir * aveTime;

	// extract left and right
	#left, right = stereoIn;

	// running sum square
	left2 = RunningSum.ar(left.squared, numsamp);
	right2 = RunningSum.ar(right.squared, numsamp);

	// running sum multiply
	multLR = RunningSum.ar(left * right, numsamp);

	// the regularization - avoid divide by zero!
	reg = DC.ar(regGain.dbamp);

	// analyze balance
	correlation = 2 * multLR / ((left2 + right2) + reg);

	// return
	correlation;
};


// function to calculate time-average encoding angle (in radians) of a stereophonic signal
//
tAAngle = { |stereoIn, aveTime = 0.05, regGain = -180.0|

	var left, right;
	var numsamp;
	var left2, right2, multLR, angle;
	var reg;

	// convert time to numsamps
	numsamp = SampleRate.ir * aveTime;

	// extract left and right
	#left, right = stereoIn;

	// running sum square
	left2 = RunningSum.ar(left.squared, numsamp);
	right2 = RunningSum.ar(right.squared, numsamp);

	// running sum multiply
	multLR = RunningSum.ar(left * right, numsamp);

	// the regularization - avoid divide by zero!
	reg = DC.ar(regGain.dbamp);

	// analyze angle
	angle = 0.5 * atan2(left2 - right2, (2 * multLR) + reg);

	// return
	angle;
};


// a function to return coefficients for Width- Matrix form
// width argument in degrees
widthMatrix = { |angle = 0|
    var theta = angle.degrad;

    Array2D.fromArray(
        2, // rows, outputs
        2, // columns, inputs
        [
            theta.cos, -1 * theta.sin,
            -1 * theta.sin, theta.cos
        ]
    )
};


// a function to return coefficients for Rotate- Matrix form
// position argument in degrees
rotateMatrix = { |angle = 0|
    var theta = angle.degrad;

    Array2D.fromArray(
        2, // rows, outputs
        2, // columns, inputs
        [
            theta.cos, theta.sin,
            -1 * theta.sin, theta.cos
        ]
    )
};


// spatial filter - a function we'll use inside our synthDef below...
spatFilt = { |in, coeffMatrix|

	// wrap input as array if needed --> for mono inputs
	in.isArray.not.if({
		in = Array.with(in)
	});

	Mix.fill(coeffMatrix.cols, { |i| // fill input
		UGen.replaceZeroesWithSilence(
			coeffMatrix.asArray.reshape(coeffMatrix.rows, coeffMatrix.cols).flop.at(i) * in.at(i)
		)
	})
};


///////////////// DEFINE SYNTHS //////////////////

pinkSynthDef = CtkSynthDef.new(\pink, { |dur, gain = 0.0, ris = 0.01, dec = 0.01, panAngle = 0.0, widthAngle = 0.0|
    var bus = 0;      // var to specify output bus: first output
    var amp;          // a few vars for synthesis
    var sig, out;     // vars assigned to audio signals
    var ampEnv;       // var for envelope signal


    // calcs
    amp = gain.dbamp;  // convert from gain in dB to linear amplitude scale

    // the amplitude envelope nested in the UGen that synthesises the envelope
    ampEnv = EnvGen.kr(
        Env.linen(ris, 1.0 - (ris + dec), dec),
        timeScale: dur
    );

    // generate test signal, decorrelated PinkNoise
	sig = PinkNoise.ar([1, 1]);  // <-- Panning happens here!

    // spatial filter - width & rotate
    sig = spatFilt.value(sig, widthMatrix.value(widthAngle));  // <-- And here!
    sig = spatFilt.value(sig, rotateMatrix.value(panAngle));  // <-- And here!

	// post pan angle (for display)
	panAngle.poll(label: "pan_____");  // <-- Quote this out if you don't want to post the angle

	// apply envelope
	sig = amp * ampEnv * sig;

    // imaged
    out = sig;

    // out!!
    Out.ar(bus, out)
});


// a simple synthDef to analize the stereo image
// in terms of stereo balance and correlation
// we'll use SC3's X/Y scope to view!
analyDef = CtkSynthDef.new(\analySynth, { |dur, aveTime = 0.005, radius = 0.1, receiveBus = 0, dispBus|
    var inSig, dispSig;
    var numChannels = 2;
	var circle;

	// generate "noisy circle"
	circle = (SinOsc.ar(ControlRate.ir, [pi/2, 0.0]) * LFNoise2.ar(ControlRate.ir, [1, 1]));

    // receive test signal
    inSig = In.ar(receiveBus, numChannels);

	// analysis - find angle (for display)
	(tAAngle.value(inSig, aveTime)*180/pi).poll(label: "analyzed");  // <-- Quote this out if you don't want to post the angle

	// analysis - find balance & correlation
	dispSig = Array.with(tABalance.value(inSig, aveTime), tACorrelation.value(inSig, aveTime));  // the "actual" analysis
	dispSig = ((1.0 - radius) * dispSig) + (radius * circle);  // add the "noisy circle"
	dispSig = [-1, 1] * dispSig;  // remap for X/Y display

    // outputs here
    Out.ar(
        dispBus, // analysis send out
        dispSig
    );
});


///////////////// SET VARIABLE VALUES //////////////////

// create a score
score = CtkScore.new;


// create the sendBus
dispBus = CtkAudio.new(2); // a two channel (stereo) bus...
                           // ... which is what we'll be sending!

// create a node group
group = CtkGroup.new;


///////////////// SET PARAMETER VALUES //////////////////

// common values...
start = 0.0;
dur = 20.0;
gain = -6.0;


// Example 1
//
// mono pink noise - pan from -S to +S
//
// same as PAN!
//
panAngle = CtkControl.env(Env.new([-90.0, 90.0], [1.0], \lin), timeScale: dur);
widthAngle = -45.0;


// // Example 2
// //
// // partially decorrelated pink noise - pan from -S to +S
// //
// // ROTATE a stereo image
// //
// panAngle = CtkControl.env(Env.new([-90.0, 90.0], [1.0], \lin), timeScale: dur);
// widthAngle = -22.5;


// // Example 3
// //
// // mono - decorrelated - mono pink noise - pan from +R to +L
// //
// // same as BALANCE!
// //
// panAngle = -45.0;
// widthAngle = CtkControl.env(Env.new([-45.0, 45.0], [1.0], \lin), timeScale: dur);


// // Example 4
// //
// // mono - decorrelated - mono pink noise - pan from +M to +S
// //
// // same as WIDTH!
// //
// panAngle = 0.0;
// widthAngle = CtkControl.env(Env.new([-45.0, 45.0], [1.0], \lin), timeScale: dur);


///////////////// POPULATE THE SCORE //////////////////

// add to score
score.add(group);
score.add(dispBus);

// define the note / add to score
// a single note
score.add(
	pinkSynthDef.note(starttime: start, duration: dur)
	.dur_(dur)
	.gain_(gain)
	.panAngle_(panAngle)
	.widthAngle_(widthAngle)
);




// analysis
score.add(
    analyDef.note(starttime: start, duration: dur, addAction: \tail, target: group)
    .dur_(dur)
    .dispBus_(dispBus)
);


// view the analysis, using the X/Y scope
s.scope(2, index: dispBus.bus).style = 2;

///////////////// PLAY THE SCORE //////////////////

// play the score
score.play;


// free
dispBus.free;
)
::


section:: Further Help

list::
## link::Help::
::
