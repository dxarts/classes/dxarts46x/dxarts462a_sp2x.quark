title:: 06b. Delay Lines - Private Bussing
summary:: DXARTS 462 - Week 6
categories:: Tutorials>DXARTS>462
keyword:: DXARTS

link::Tutorials/DXARTS-462/00-DXARTS-462-TOC## 00. DXARTS 462: Digital Sound Processing::

link::Tutorials/DXARTS-462/06-schelp/06a-Delay-Lines-Bussing-and-Feedback## << 06a. Delay Lines - Feedforward, Feedback & Local Bussing:: link::Tutorials/DXARTS-462/06-schelp/06c-Delay-Lines-Vibrato## >> 06c. Delay Lines - Vibrato (& Tremelo)::


In link::Tutorials/DXARTS-462/06-schelp/06a-Delay-Lines-Bussing-and-Feedback## 06a. Delay Lines - Bussing & Feedback:: we link::Tutorials/DXARTS-462/06-schelp/06a-Delay-Lines-Bussing-and-Feedback#Delay,%20LocalIn%20&%20LocalOut%20Example#introduced:: emphasis::local bussing::, bussing within a single UGen graph. Here we'll begin to review emphasis::private bussing::, which facilitates bussing between multiple UGen graphs. We'll find this idiom particularly useful for creating effects that are intended to act on a global mix of some sort. One example could be the output of many UGen graphs mixed and then sent to another providing reverberation.

____________________________________

Let's start by booting the server:

code::
s.boot
::

Opening the scopes:

code::
// display
(
var numChannels = 1;

Stethoscope.new(s, numChannels);
FreqScope.new;
)
::

And making the associated link::Classes/MainParadigm:: projects:

code::
(
~tutorialName = "06b-Delay-Lines-Private-Bussing";  // this tutorial page name
~courseQuark = "dxarts462a_sp2x";  // the course name

// make the project
MainParadigm.new46xExamples(~tutorialName, ~courseQuark)
)
::


section:: Private Bussing

We'll start with being more explicit with bussing. And, actually, we've been using bussing all along!

Let's have a look at the help for link::Classes/Out::. We see that the first argument for link::Classes/Out:: is an "index of the bus to write out to." We've been setting this to code::0:: to emphasis::write:: footnote::route:: to the first output(s) on our audio hardware. footnote::SuperCollider has both audio busses and control (rate) busses. For now, we'll focus on audio bussing.::

For audio, there are three kinds of busses. From the help for link::Classes/Bus::, the kinds and their indices:

numberedlist::
##strong::Hardware output buses::: code::0 .. (numOutputBusChannels - 1)::
##strong::Hardware input buses::: code::numOutputBusChannels .. (numOutputBusChannels + numInputBusChannels - 1)::
##strong::First private bus index::: code::numOutputBusChannels + numInputBusChannels::
::

link::Browse#Libraries%3EComposition%20ToolKit#Ctk:: includes convenient handling of private bussing via link::Classes/CtkAudio::. We'll use this feature to manage our emphasis::private bussing::.

Additionally, in order to make sure our delays process the desired input signal, we'll need to create a link::Classes/Node:: link::Classes/Group::. Doing so will allow us to assign the delays to execute emphasis::after:: the initial signal generation, and therefore process our generated signal.

We'll use the code::addAction: \tail:: message to do this.

subsection::Equal Power Mixing

Once emphasis::SOURCE:: and emphasis::FILTER:: are split into different UGen graphs, the notion of emphasis::mixing:: is useful to control the balance between the original and further processed signal. emphasis::Equal power mixing:: is a good choice:

image::06b_eq01.png::

Given emphasis::kmix:: between code::0-1:: scalars are returned for the emphasis::direct:: and the emphasis::delayed::, or otherwise processed, signal.

Here's a function to return these equal power coefficients:

code::
~equalPowCoeffs= { |mix = 0.5|

    Array.with((1 - mix).sqrt, mix.sqrt)
};
::


subsection::Private Bussing Example

We'll adapt the UGen graphs from link::Tutorials/DXARTS-462/06-schelp/06a-Delay-Lines-Bussing-and-Feedback##ealier::, splitting our emphasis::SOURCE:: and emphasis::FILTER:: into two different UGen graphs. As a result, we'll have to manage some bookkeeping making sure the emphasis::FILTER::s are kept "alive" until they've finished returning signal.

As we've been processing existing sounds loaded into a link::Classes/CtkBuffer::, we'll adapt the link::Classes/PlayBuf:: design link::Tutorials/DXARTS-462/01-schelp/01b-Sound-Files-Getting-Sound-In#Play%20from%20memory%20with%20PlayBuf#we've been using:: as our emphasis::SOURCE::.

note::
We've added an argument for the bus we'll send to. Also, notice the corresponding link::Classes/Out::.
::

code::
{| dur, gain = 0.0, ris = 0.01, dec = 0.01, rate = 1, panAngle = 0.0, loop = 0, buffer = 0, sendGain = 0.0, sendBus|

	// variables
	var bus;          // var to specify output bus
	var numChannels = 1; // <-- mono buffer
	var playBuf, directOut, sendOut;     // vars assigned to audio signals
	var directAmp, sendAmp;  // a few vars for synthesis
	var ampEnv;       // vars for envelope signal


	// assign values
	bus = 0;          // first output

	// calcs
	directAmp = gain.dbamp; // convert from gain in dB to linear amplitude scale
	sendAmp = sendGain.dbamp;

	// amplitude envelope: Env nested in EnvGen
	ampEnv = EnvGen.kr(
		Env.linen(ris, 1.0 - (ris + dec), dec),  // <-- envelope
		timeScale: dur
	);

	// sample playback
	playBuf = PlayBuf.ar(numChannels, buffer,  BufRateScale.kr(buffer) * rate, loop: loop);

	// rescale, by multiplying by ampEnv
	playBuf = ampEnv * playBuf;

	// assign direct and send out
	directOut = directAmp * main.functions[\sinCosPanLaw].value(panAngle) * playBuf;  // <-- Panning happens here!
	sendOut = sendAmp * playBuf;

	// out!!
	Out.ar(
		bus, // main out
		directOut
	);
	Out.ar(
		sendBus, // effects send out
		sendOut
	)
}
::

For our emphasis::FILTER::s, we'll adapt the designs introduced for link::Tutorials/DXARTS-462/06-schelp/06a-Delay-Lines-Bussing-and-Feedback#Delay%20Example#Slapback Echo:: and link::Tutorials/DXARTS-462/06-schelp/06a-Delay-Lines-Bussing-and-Feedback#Comb%20Example#Feedback Echo::. The modified emphasis::Feedback Echo:: is listed below.

note::
We've added an argument for the bus we'll receive from. Also, notice the corresponding link::Classes/In::.
::

code::
{| dur, gain = 0.0, ris = 0.01, dec = 0.01, mix = 1.0, directPanAngle = 0.0, delayPanAngle = 0.0, delayTime = 0.2, maxDelayTime = 0.2, receiveBus|

	// variables
	var bus;          // var to specify output bus
	var numChannels = 1; // <-- mono bus
	var direct, delay, out;     // vars assigned to audio signals
	var amp;  // a few vars for synthesis
	var ampEnv;  // var for envelope signal
	var directAmp, delayAmp;  // comb coefficients: b0, bM

	// assign values
	bus = 0;          // first output

	// calcs
	amp = gain.dbamp; // convert from gain in dB to linear amplitude scale

	// delay (FIR Comb) filter calcs - direct & delay scalar (b0, bM)
	#directAmp, delayAmp = main.functions[\equalPowCoeffs].value(mix);

	// amplitude envelope: Env nested in EnvGen
	ampEnv = EnvGen.kr(
		Env.linen(ris, 1.0 - (ris + dec), dec),  // <-- envelope
		timeScale: dur
	);

	// read sound in - from receive bus
	direct = In.ar(receiveBus, numChannels);

	// delay
	// NOTE: we could have added the direct signal here
	//       but have kept separate so the direct and delayed
	//       signal can be panned separately
	delay = DelayC.ar(direct, maxDelayTime, delayTime);

	// add panned direct and delay to out, & then envelope
	out = amp * ampEnv * (
		(main.functions[\sinCosPanLaw].value(directPanAngle) * directAmp * direct) +  // <-- direct
		(main.functions[\sinCosPanLaw].value(delayPanAngle) * delayAmp * delay)  // <-- delay
	);

	// out!!
	Out.ar(bus, out)
}
::

strong::Setting up bussing::

We'll need to instantiate the emphasis::private bus::. We'll use link::Classes/CtkAudio:: to do so. Also, we'll need to create a link::Classes/Group::, and add to the score. For convenience, we'll use link::Classes/CtkGroup:::

code::
///////////////// SET UP BUSSING //////////////////

// create a private bus
delayBus = CtkAudio.new(1); // a single channel bus...
// ... which is what we'll be sending!

// create a node group, and add to score
delayGroup = CtkGroup.new;
main.score.add(delayGroup);
::

With these aspects in place, we're ready to go. In the codeblock below, you'll see we first create a emphasis::SOURCE:: note event, sending to a emphasis::private bus:: named code::delayBus::. Two emphasis::FILTER::s, emphasis::Slapback Echos::, are then instanced, receiving from code::delayBus::.

note::
The emphasis::SOURCE:: is added to the head of code::delayGroup::, while the emphasis::FILTER::s are added to the tail.

strong::QUESTION::: Why is this necessary?
::

code::
// EXAMPLE 2
//
// Two Slapback Echos, via private bussing

// direct in center - send to the delay bus
main.score.add(
	main.synthDefs[\prMonoPlayBufLinenPan].note(
		starttime: 4.0,
		duration: pizzBuf.duration,
		addAction: \head,  // add to head...
		target: delayGroup  // ... of delay group
	)
	.dur_(pizzBuf.duration)
	.gain_(12.0)
	.sendGain_(12.0)
	.panAngle_(0.0)
	.buffer_(pizzBuf)
	.sendBus_(delayBus)
);


// ... two slapback echos...
main.score.add(
	// slapback 1
	// delay = 0.3
	main.synthDefs[\prSlapBackMonoBusLinenPan].note(
		starttime: 4.0,
		duration: pizzBuf.duration + 0.3,  // duration + delay time
        addAction: \tail,  // add to tail...
		target: delayGroup  // ... of delay group
	)
	.dur_(pizzBuf.duration + 0.3)  // duration + delay time
	.delayPanAngle_(45)
	.delayTime_(0.3)
	.maxDelayTime_(0.3)
	.receiveBus_(delayBus),

	// slapback 2
	// delay = 0.7
	main.synthDefs[\prSlapBackMonoBusLinenPan].note(
		starttime: 4.0,
		duration: pizzBuf.duration + 0.7,
        addAction: \tail,  // add to tail...
		target: delayGroup  // ... of delay group
	)
	.dur_(pizzBuf.duration + 0.7)
	.delayPanAngle_(-45)
	.delayTime_(0.7)
	.maxDelayTime_(0.7)
	.receiveBus_(delayBus),
);
::

Let's go ahead and open up the associated project file and render this example:

code::
(
~tutorialName = "06b-Delay-Lines-Private-Bussing";  // this tutorial page name
~exampleName = "01-private-bussing-example";  // example project

Document.open(
	"~/Desktop/%/%/%.scd".format(
		~tutorialName.toLower,
		~exampleName,
		~exampleName
	).standardizePath
)
)
::

Evaluate the codeblock in this open file, teletype::01-private-bussing-example.scd::.

The examples here illustrate link::Tutorials/DXARTS-462/06-schelp/06a-Delay-Lines-Bussing-and-Feedback#FIR%20Comb:%20Slapback%20Echo#Slapback:: and link::Tutorials/DXARTS-462/06-schelp/06a-Delay-Lines-Bussing-and-Feedback#IIR%20Comb:%20Feedback%20Echo#Feedback Echo::, in a emphasis::private bussing:: implementation.

____________________

emphasis::Private bussing:: offers the opportunity to send and receive signals between UGen graphs. The trick is setting up a (link::Classes/Node::) link::Classes/Group:: and making sure these signals are passed around in the correct order. Here we've only set up one single channel effects bus.

As you'd imagine, we can set up multiple effects busses. These can be single channel, as above, or multi-channel. We can get fairly complicated with our bussing, if we desire, making our UGen graphs both send and receive to emphasis::private busses:: set up by link::Classes/CtkAudio::.


section:: Further Help

list::
## link::Help::
::
