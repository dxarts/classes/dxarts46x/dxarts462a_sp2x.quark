title:: 06c. Delay Lines - Vibrato (& Tremelo)
summary:: DXARTS 462 - Week 6
categories:: Tutorials>DXARTS>462
keyword:: DXARTS

link::Tutorials/DXARTS-462/00-DXARTS-462-TOC## 00. DXARTS 462: Digital Sound Processing::

link::Tutorials/DXARTS-462/06-schelp/06b-Delay-Lines-Private-Bussing## << 06b. Delay Lines: Private Bussing:: link::Tutorials/DXARTS-462/06-schelp/06d-Windowed-Delays-Time-segment-Processing## >> 06d. Windowed Delays - Time-segment Processing / Granular Synthesis::

Adding various forms of emphasis::delay line modulation:: to our repertoire opens up a variety of useful signal processing techniques. Earlier, we explored both link::Tutorials/DXARTS-461/04-schelp/04a-AM-Modulation##Amplitude Modulation (AM):: and link::Tutorials/DXARTS-461/05-schelp/05-FM-Modulation##Frequency Modulation (FM):: in the context of syntesized signals. Given emphasis::delay line modulation:: we'll see how these techniques can be applied in a signal processing context.

____________________________________

Let's start by booting the server:

code::
s.boot
::

Opening the scopes:

code::
// display
(
var numChannels = 1;

Stethoscope.new(s, numChannels);
FreqScope.new;
)
::

And making the associated link::Classes/MainParadigm:: projects:

code::
(
~tutorialName = "06c-Delay-Lines-Vibrato";  // this tutorial page name
~courseQuark = "dxarts462a_sp2x";  // the course name

// make the project
MainParadigm.new46xExamples(~tutorialName, ~courseQuark)
)
::


section:: Vibrato: Phase Modulation

It is possible to implement emphasis::vibrato:: as a signal processing technique in a variety of ways. Dodge & Jerse introduce emphasis::vibrato:: in the context of synthesis footnote::Chapter 4.8C, page 94.:: as a "slight wavering of pitch". Their illustrated network footnote::Figure 4.19, page 95.:: is a emphasis::Frequency Modulation (FM):: architecture. They make the point that the emphasis::modulation frequency:: must be low, otherwise the familiar link::Tutorials/DXARTS-461/05-schelp/05-FM-Modulation#Spectral%20relation%20to%20RM%20&%20AM#sidebands of FM:: are generated. Recall, it was briefly mentioned that link::Tutorials/DXARTS-461/05-schelp/05-FM-Modulation#footnote_7#Frequency Modulation and Phase Modulation (PM) are closely related::. footnote::We used the time-domain expression for Phase Modulation in our discussion of Frequency Modulation, as is the convention in the literature.:: Given the relationship of FM and PM, it shouldn't be too difficult to imagine that if vibrato can be successfully implemented as FM, it can likely be implemented as PM.


subsection:: Time & Phase

It is very easy to get confused here when we speak of emphasis::time:: and emphasis::phase::. We've often used these interchangably, thinking in these terms:

table::
## || strong::range:: || strong::units:: || strong::scale::
##emphasis::time:: || code::0:: to code::+inf:: || seconds || large (sound object)
##emphasis::phase:: || code::0:: to code::2pi:: || radians || small (waveform)
::

Let's review briefly the link::Tutorials/DXARTS-461/04-schelp/04a-AM-Modulation#Sinusoids#instaneous time representation of a sinusoid:::

image::06c_eq01.png::

Where:

image::06c_eq02.png::

We can then expand the expression for emphasis::instaneous phase::: footnote::You may like to review: Picinbono, B. link::http://ieeexplore.ieee.org/document/558469/##"On Instantaneous Amplitude and Phase of Signals.":: Signal Processing, IEEE Transactions on 45, no. 3 (1997): 552-60.::

image::06c_eq03.png::

What we're seeing here is how emphasis::instaneous phase:: and emphasis::time:: are related for a sinusoid. We should note that if emphasis::time:: is somehow modulated, the emphasis::instantaneous phase:: will also be modulated.

Conveniently, SuperCollider's delay UGens offer the option to modulate the delay line length.


subsection:: Decimation & Interpolation

We've link::Tutorials/DXARTS-462/01-schelp/01b-Sound-Files-Getting-Sound-In#PlayBuf#already experienced:: soundfile emphasis::resampling:: using link::Classes/PlayBuf::'s code::rate:: argument. Technically, emphasis::resampling:: is a combination of emphasis::decimation:: and emphasis::interpolation::. footnote::Crochiere, R.E., and L.R. Rabiner. link::http://ieeexplore.ieee.org/document/1456237/##"Interpolation and Decimation of Digital Signals — A Tutorial Review.":: Proceedings of the IEEE 69, no. 3 (1981): 300-31.:: emphasis::Resampling:: is often described as emphasis::sample rate conversion::. If we retain the same sample rate, we can characterize the processess as: footnote::
Some other useful discussions on the topic of emphasis::decimation:: & emphasis::interpolation:: can be found here:
list::
##link::http://dspguru.com/dsp/faqs/multirate/interpolation::
##link::http://dspguru.com/dsp/faqs/multirate/decimation::
##link::https://en.wikipedia.org/wiki/Interpolation::
##link::https://en.wikipedia.org/wiki/Decimation_(signal_processing)::
::
::

table::
## || strong::samples:: || strong::time modulation:: || strong::playback rate:: || strong:: pitch modulation::
##emphasis::decimation:: || discard || decrease || code::> 1:: || increase
##emphasis::interpolation:: || repeat || increase || code::< 1:: || decrease
::

Given our link::Tutorials/DXARTS-462/06-schelp/06c-Delay-Lines-Vibrato#Time%20&%20Phase#observation above::, it should be apparent that if we can modulate emphasis::time::, we can modulate emphasis::phase::, and therefore create emphasis::vibrato::.


subsection:: Modulating Time

When we modulate the emphasis::delay time:: argument of a emphasis::delay line:: we are emphasis::modulating time::. That is we are either emphasis::decimating:: or emphasis::interpolating:: the signal found in the delay's buffer. As the emphasis::delay tap:: is modulated towards the begining of the delay line the signal is emphasis::decimated::. When it moves towards the end, the signal is emphasis::interpolated::.

Conveniently, Dattorro footnote::Dattorro, Jon. link::https://ccrma.stanford.edu/~dattorro/EffectDesignPart2.pdf##"Effect Design: Part 2: Delay-line Modulation and Chorus.":: AES: Journal of the Audio Engineering Society 45, no. 10 (1997): 764-88.:: has provided an equation for the emphasis::instaneous resampling ratio:: returned by a sinusoid modulator: footnote::Zolzer lists this as equation 3.14, page 89. Note: code::2*DEPTH:: is the length of the delay line, in seconds.::

image::06c_eq04.png::

Where emphasis::T:: is the length of the delay line, in seconds, being traversed. Here we see how the length of the delay relates to the emphasis::instaneous resampling ratio::.

This isn't quite enough information to build the algorithm. Let's assume that a reasonable way to control the resampling is to specify the emphasis::maximum resampling ratio::, emphasis::a::: footnote::Zolzer is using emphasis::a:: as the emphasis::instaneous resampling ratio:: in equations 3.14 & 3.16, page 89.::

image::06c_eq05.png::

emphasis::Maximum resampling ratio::, emphasis::a::, must have a value greater than or equal to code::1::, and is the maximum upward transposition ratio to be applied.

We can now solve for emphasis::T::, the length of the delay line traversed:

image::06c_eq06.png::

And then express emphasis::T:: in terms of the emphasis::modulation frequency::, emphasis::f::, directly: footnote::If we wish to specify vibrato in terms of emphasis::vibrato width::, emphasis::W::, emphasis::T:: would be calculated as: image::06c_eq08.png:: This is the form employed by Dodge & Jerse. ::

image::06c_eq07.png::

Written as a function:

code::
// function to return delay line length, T,
//  to be traversed by sinusoidal modulator
//
// maxRatio: maximum resampling ratio, a
// modRate: modulation rate, f
~sinVibDelayLength = { |maxRatio, modRate|
    var delayLength;

    delayLength = (maxRatio - 1) / (pi * modRate);

	// return (in seconds)
    delayLength;
};

// test
~sinVibDelayLength.value(maxRatio: 1.01, modRate: 6.0);
~sinVibDelayLength.value(maxRatio: 1.02, modRate: 6.0);  // increasing transposition - need more delay time
~sinVibDelayLength.value(1.06, 6.0);  // increasing transposition - need more delay time

~sinVibDelayLength.value(maxRatio: 1.01, modRate: 6.0);
~sinVibDelayLength.value(maxRatio: 1.01, modRate: 3.0);  // decreasing vibrato rate - need more delay time
~sinVibDelayLength.value(maxRatio: 1.01, modRate: 1.0);  // decreasing vibrato rate - need more delay time
::

Here we see that we'll need a longer delay line, emphasis::T::, as:
list::
##the emphasis::maximum resampling ratio::, emphasis::a::, increases
##the emphasis::modulation frequency::, emphasis::f::, decreases
::

subsection::Vibrato Example

Here's the delay line vibrato UGen graph we'll use:

code::
{| dur, gain = 0.0, ris = 0.01, dec = 0.01, panAngle = 0.0, modRate = 6.0, minModRate = 6.0, ratio = 1.01, maxRatio = 1.01, receiveBus|

	// variables
	var bus;          // var to specify output bus
	var numChannels = 1; // <-- mono bus
	var in, delay, out;     // vars assigned to audio signals
	var amp;  // a few vars for synthesis
	var ampEnv;  // var for envelope signal
	var vibLFO;
	var delayLength, maxDelayLength;

	// assign values
	bus = 0;          // first output

	// calcs
	amp = gain.dbamp; // convert from gain in dB to linear amplitude scale

	// vibrato calcs
	delayLength = main.functions[\sinVibDelayLength].value(ratio, modRate);
	maxDelayLength = main.functions[\sinVibDelayLength].value(maxRatio, minModRate);

	// amplitude envelope: Env nested in EnvGen
	ampEnv = EnvGen.kr(
		Env.linen(ris, 1.0 - (ris + dec), dec),  // <-- envelope
		timeScale: dur
	);

	// read sound in - from receive bus
	in = In.ar(receiveBus, numChannels);

	// delay line modulator (unscaled)
	// NOTE: this could be SinOsc.kr
	vibLFO = SinOsc.ar(modRate, pi/2);

	// modulated delay line - vibrato!
	delay = DelayC.ar(in, maxDelayLength, vibLFO.range(0, 1) * delayLength);


	// scale, envelope, pan & out
	out = amp * ampEnv * main.functions[\sinCosPanLaw].value(panAngle) * delay;  // <-- delay

	// out!!
	Out.ar(bus, out)
}
::

Let's go ahead and open up the associated project file and render this example:

code::
(
~tutorialName = "06c-Delay-Lines-Vibrato";  // this tutorial page name
~exampleName = "01-vibrato-example";  // example project

Document.open(
	"~/Desktop/%/%/%.scd".format(
		~tutorialName.toLower,
		~exampleName,
		~exampleName
	).standardizePath
)
)
::

Evaluate the codeblock in this open file, teletype::01-vibrato-example.scd::.

The first several examples call a function named code::vibScale::, which iterates through a collection of modulation rates and ratios:

code::
main.functions[\vibScale].value(
	main: main,
	start: 0.0,
	gain: 12.0,
	buffer: fluteBuf,
	modRates: [ 6.0, 6.0, 6.0, 6.0, 6.0, 6.0 ],
	modRatios: [ 1.0, 1.007, 1.01, 1.03, 1.0625, 1.125 ],
	group: vibratoGroup,
	bus: vibratoBus
);
::

The last example, offers a more natural sounding result by enveloping the modulation rate and ratio:

code::
/*
Use Envelopes - sounds nicer!
*/

// create a note, and set arguments - SEND
main.score.add(
	main.synthDefs[\prMonoPlayBufLinenPan].note(
		starttime: 23.0,
		duration: fluteBuf.duration,
		addAction: \head,  // add to head...
		target: vibratoGroup  // ... of vibrato group
	)
	.dur_(fluteBuf.duration)
	.gain_(-inf)  // "mute" direct signal
	.sendGain_(0.0)  // "send"
	.buffer_(fluteBuf)
	.sendBus_(vibratoBus);
);

// create a note, and set arguments - RECEIVE
main.score.add(
	main.synthDefs[\prVibMonoBusLinenPan].note(
		starttime: 23.0,
		duration: fluteBuf.duration,
		addAction: \tail,  // add to tail...
		target: vibratoGroup  // ... of vibrato group
	)
	.dur_(fluteBuf.duration)
	.gain_(12.0)
	.modRate_(  // envelope modulation rate
		CtkControl.env(
			Env.new(
				[ 3.0, 6.0, 7.0, 3.0, 3.0 ],  // modulation rates
				[ 0.5, 0.2, 0.1, 0.2 ],  // times
				\exp),
			timeScale: fluteBuf.duration)
	)
	.minModRate_(3.0)  // need the minimum rate here, from the above envelope!!!
	.ratio_(  // envelope on ratio
		CtkControl.env(
			Env.new(
				[ 1.0, 1.007, 1.01, 1.0, 1.0 ],  // modulation ratios
				[ 0.5, 0.2, 0.1, 0.2 ],  // times
				\exp),
			timeScale: fluteBuf.duration)
	)
	.maxRatio_(1.01)  // need the maximum ratio here, from the above envelope!!!
	.receiveBus_(vibratoBus);
);
::


section::Vibrato & Tremelo: Phase & Amplitude Modulation

We'll finish off this design by adding emphasis::tremelo::, using the link::Tutorials/DXARTS-461/04-schelp/04a-AM-Modulation#AM%20Type%202:%20Complex%20Carrier,%20Simple%20Modulator#Amplitude Modulation design:: from earlier. footnote::See the network illustrated in Dodge & Jerse figure 4.13, page 91.::

 Employing a link::https://en.wikipedia.org/wiki/In-phase_and_quadrature_components##quadrature:: oscillator pair for emphasis::vibrato:: (PM) and emphasis::tremelo:: (AM), will return a more natural sounding result.


subsection::Vibrato & Tremelo Example

Here's the UGen graph we'll use. You'll notice we've added an argument for the AM sideband gain, code::modIndexGain:::

code::
{| dur, gain = 0.0, ris = 0.01, dec = 0.01, panAngle = 0.0, modRate = 6.0, minModRate = 6.0, ratio = 1.01, maxRatio = 1.01, modIndexGain = -12.0, receiveBus|

	// variables
	var bus;          // var to specify output bus
	var numChannels = 1; // <-- mono bus
	var in, delay, out;     // vars assigned to audio signals
	var amp;  // a few vars for synthesis
	var ampEnv;  // var for envelope signal
	var modLFO;
	var delayLength, maxDelayLength;
	var modIndex;     // AM modulation index (a scalar)
	var normFac;      // AM normalization factor

	// assign values
	bus = 0;          // first output

	// calcs
	amp = gain.dbamp; // convert from gain in dB to linear amplitude scale

	// vibrato (FM) calcs
	delayLength = main.functions[\sinVibDelayLength].value(ratio, modRate);
	maxDelayLength = main.functions[\sinVibDelayLength].value(maxRatio, minModRate);

	// tremelo (AM) calcs
	modIndex = modIndexGain.dbamp; // convert AM gain in dB to linear amplitude scale
	normFac = (1 + (2*modIndex)).reciprocal; // AM amplitude normalization factor


	// amplitude envelope: Env nested in EnvGen
	ampEnv = EnvGen.kr(
		Env.linen(ris, 1.0 - (ris + dec), dec),  // <-- envelope
		timeScale: dur
	);

	// read sound in - from receive bus
	in = In.ar(receiveBus, numChannels);

	// delay line & amplitude modulator (unscaled)
	// NOTE: this could be SinOsc.kr
	modLFO = SinOsc.ar(modRate, [ pi/2, 0 ]); // quadrature modulator

	// modulated delay line
	delay = DelayC.ar(in, maxDelayLength, modLFO.at(0).range(0, 1) * delayLength);  // vibrato
	delay = normFac * (1 + (modIndex * modLFO.at(1))) * delay;  // tremelo

	// scale, envelope, pan & out
	out = amp * ampEnv * main.functions[\sinCosPanLaw].value(panAngle) * delay;  // <-- delay

	// out!!
	Out.ar(bus, out)
}
::

Let's go ahead and open up the associated project file and render this example:

code::
(
~tutorialName = "06c-Delay-Lines-Vibrato";  // this tutorial page name
~exampleName = "02-vibrato-&-tremelo-example";  // example project

Document.open(
	"~/Desktop/%/%/%.scd".format(
		~tutorialName.toLower,
		~exampleName,
		~exampleName
	).standardizePath
)
)
::

Evaluate the codeblock in this open file, teletype::02-vibrato-&-tremelo-example.scd::.

The first several examples call a function named code::vibTremScale::. incresing emphasis::tremelo:: is added with each subsequent note:

code::
main.functions[\vibTremScale].value(
	main: main,
	start: 0.0,
	gain: 12.0,
	buffer: fluteBuf,
	modRates: [ 6.0, 6.0, 6.0, 6.0, 6.0, 6.0 ],
	modRatios: [ 1.0, 1.007, 1.007, 1.007, 1.007, 1.007 ],
	modIndexGains: [ -inf, -30.0, -24.0, -18.0, -12.0, -6.0 ],  // adding more tremelo (AM)
	group: vibratoGroup,
	bus: vibratoBus
);
::

As above, the last example, offers a more natural sounding result by enveloping the emphasis::vibrato:: and emphasis::tremelo:: parameters:

code::
/*
Use Envelopes - sounds nicer!
*/

// create a note, and set arguments - SEND
main.score.add(
	main.synthDefs[\prMonoPlayBufLinenPan].note(
		starttime: 23.0,
		duration: fluteBuf.duration,
		addAction: \head,  // add to head...
		target: vibratoGroup  // ... of vibrato group
	)
	.dur_(fluteBuf.duration)
	.gain_(-inf)  // "mute" direct signal
	.sendGain_(0.0)  // "send"
	.buffer_(fluteBuf)
	.sendBus_(vibratoBus);
);

// create a note, and set arguments - RECEIVE
main.score.add(
	main.synthDefs[\prVibTremMonoBusLinenPan].note(
		starttime: 23.0,
		duration: fluteBuf.duration,
		addAction: \tail,  // add to tail...
		target: vibratoGroup  // ... of vibrato group
	)
	.dur_(fluteBuf.duration)
	.gain_(12.0)
	.modRate_(  // envelope modulation rate
		CtkControl.env(
			Env.new(
				[ 3.0, 6.0, 7.0, 3.0, 3.0 ],  // modulation rates
				[ 0.5, 0.2, 0.1, 0.2 ],  // times
				\exp),
			timeScale: fluteBuf.duration)
	)
	.minModRate_(3.0)  // need the minimum rate here, from the above envelope!!!
	.ratio_(  // envelope on ratio
		CtkControl.env(
			Env.new(
				[ 1.0, 1.007, 1.01, 1.0, 1.0 ],  // modulation ratios
				[ 0.5, 0.2, 0.1, 0.2 ],  // times
				\exp),
			timeScale: fluteBuf.duration)
	)
	.maxRatio_(1.01)  // need the maximum ratio here, from the above envelope!!!
	.modIndexGain_(  // envelope tremelo index (dB)
		CtkControl.env(
			Env.new(
				[ -60.0, -9.0, -12.0, -60.0, -60.0 ],
				[ 0.5, 0.2, 0.1, 0.2 ],
				\sin),
			timeScale: fluteBuf.duration)
	)
	.receiveBus_(vibratoBus);
);
::

There are further modifications we might make to the above algorithm in order to realise a more natural sounding vibrato. Aside from further tweaks to the envelopes (more segments), the addition of low frequency randomisation to the various parameters will improve the algorithm. The link::Classes/LFNoise2:: UGen would be a good choice for this.

note::
Something else we might consider is generating emphasis::tremelo:: using either a Shelf Filter or a Low Pass Filter.

strong::QUESTION::: Why would we do this? How?
::

section:: Further Help

list::
## link::Help::
::
