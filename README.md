dxarts462 : Read Me
========================
_DXARTS 462 (Spring 202x): Introduction to digital sound processing._

&nbsp;

&nbsp;

Installing
==========

Distributed via [DXARTS 462 Tutorials](https://gitlab.com/dxarts/classes/dxarts46x/dxarts462a_sp2x.quark).

Start by reviewing the Quark installation instructions
[found here](https://github.com/supercollider-quarks/quarks#installing). See
also [Using Quarks](http://doc.sccode.org/Guides/UsingQuarks.html).

With [git](https://git-scm.com/) installed, you can easily install the
[DXARTS 462 Tutorials](https://gitlab.com/dxarts/classes/dxarts46x/dxarts462a_sp2x.quark)
directly by running the following line of code in SuperCollider:

    Quarks.install("https://gitlab.com/dxarts/classes/dxarts46x/dxarts462a_sp2x.quark");



Feedback and Bug Reports
========================

Known issues are logged at
[GitLab](https://gitlab.com/dxarts/classes/dxarts46x/dxarts462a_sp2x.quark/-/issues).

&nbsp;

&nbsp;



List of Changes
---------------

Version 23.5.0

* Spring 2023: week 5
  * add: 05b. include all harmonic notch example
  * refactor: 05b. AllpassC --> AllpassN, improve magnitude response plot

Version 23.0.0

* Spring 2023: week 0
  * fix: 02. Time-segment Processing - Pitch Synchronous Granular Synthesis: fix Lent values
  * fix: 04a. FIX FIR HP example
  * fix: 09a. Weaver SSB: redundant arg --> widthAngle
  * fix: LaTex equation typos
  * refactor: frequency response plots

Version 22.0.0

* Spring 2022: week 0
  * 02-Time-Segment-PSGS
    * fix: match example-1 window size to Lent's 2x period

Version 21.1.0

* Spring 2021: week 1

Version Pre-release

* Pre-release.


&nbsp;

&nbsp;

Authors
=======

&nbsp;

Copyright Joseph Anderson and the DXARTS Community, 2012-2022.

[Department of Digital Arts and Experimental Media (DXARTS)](https://dxarts.washington.edu/)
University of Washington

&nbsp;


Contributors
------------

Version 21.1.0 - 23.5.0
*  Joseph Anderson (@joslloand)

Version pre-release
*  Joseph Anderson (@joslloand)
*  Juan Pampin (@jpampin)
*  Joshua Parmenter (@joshpar)
*  Daniel Peterson (@dmartinp)
*  Stelios Manousakis (@Stylianos91)
*  James Wenlock (@wenloj)


Contribute
----------

As part of the wider SuperCollider community codebase, contributors are encouraged to observe the published SuperCollider guidelines found [here](https://github.com/supercollider/supercollider#contribute).


License
=======

The [DXARTS 462 Tutorials](https://gitlab.com/dxarts/classes/dxarts46x/dxarts462a_sp2x.quark) is free software available under [Version 3 of the GNU General Public License](https://www.gnu.org/licenses/gpl-3.0.en.html). See [LICENSE](LICENSE) for details.
